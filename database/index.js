const mongoose = require('mongoose');
const config = require('config');

const Task = require('./todo');

const MONGO_DB_URI = config.get('mongoDb.uri');

mongoose.connect(MONGO_DB_URI, { useNewUrlParser: true }, (error) => {
  if (!error) {
    // eslint-disable-next-line
    console.log('Successfully Connected to databse');
  } else {
    // eslint-disable-next-line
    console.log('Error in establishing Connection');
  }
});


module.exports = { Task };
