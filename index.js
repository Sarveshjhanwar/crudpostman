const express = require('express');
const bodyparser = require('body-parser');
require('./database');


const app = express();

app.use(bodyparser.json());

//

app.get('/', (req, res) => {
  res.send('Crud in making');
});

require('./routes/routes')(app);

app.listen(3000, () => {
  // eslint-disable-next-line
  console.log('Server started at port 3000');
});

module.exports = app;
