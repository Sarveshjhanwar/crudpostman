const Task = require('../controllers/controllers');

module.exports = (app) => {
  // Create a new Task
  app.post('/task', Task.create);

  // Retrieve all Task
  app.get('/task', Task.findAll);

  // Retrieve a single Task with TaskId
  app.get('/task/:id', Task.findOne);

  //  Update a Task with TaskId
  app.put('/task/:id', Task.update);

  // Delete a Task with TaskId
  app.delete('/task/:id', Task.delete);
};
