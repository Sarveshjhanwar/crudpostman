require('express');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');

chai.use(chaiHttp);
const requester = chai.request(server).keepOpen();


module.exports = { requester };
