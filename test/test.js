const chai = require('chai');
const { Task } = require('../database');

const bootstrap = require('./bootstrap');

const { expect } = chai;


describe('Testing for Tasks', () => {
  const { requester } = bootstrap;

  afterEach(async () => {
    await Task.deleteMany({});
  });


  // Test Get Tasks

  describe('/GET tasks', () => {
    it('it should GET all the tasks', async () => {
      const res = await requester
        .get('/task');
      expect(res).to.have.status(200);
    });
  });

  describe('/GET tasks', () => {
    it('it should GET all the tasks', async () => {
      const res = await requester
        .get('/tas');
      expect(res).to.have.status(404);
    });
  });

  describe('/Post tasks', () => {
    it('should Post the task', async () => {
      const taskPost = {
        task: 'run as fast as possible you idiot',
      };
      const res = await requester
        .post('/task')
        .send(taskPost);
      expect(res).to.have.status(200);
    });
  });

  describe('/Post tasks', () => {
    it('should return error in posting task', async () => {
      const taskPost = {
        task: '',
      };
      const res = await requester
        .post('/task')
        .send(taskPost);
      expect(res).to.have.status(400);
    });
  });

  describe('/GET/:ID', () => {
    it('should Get the task by ID', async () => {
      const tasks = new Task({ task: 'The Lord of the Rings' });
      const task = await tasks.save();
      const res = await requester
        .get(`/task/${task.id}`)
        .send(task);
      expect(res).to.have.status(200);
    });
  });

  describe('/GET/:ID', () => {
    it('should Get the task by ID', async () => {
      const tasks = new Task({ task: 'The Lord of the Rings' });
      const task = await tasks.save();
      const res = await requester
        .get(`/task/"1${task.id}"`)
        .send(task);
      expect(res).to.have.status(404);
    });
  });

  describe('/PUT/:ID task', () => {
    it('it should UPDATE a task given the id', async () => {
      const tasks = new Task({ task: 'The Chronicles of Narnia' });
      const task = await tasks.save();
      const res = await requester
        .put(`/task/${task.id}`)
        .send({ task: 'The Chronicles of Sarvesh' });
      expect(res).to.have.status(200);
    });
  });

  describe('/PUT/:ID task', () => {
    it('it should UPDATE a task given the id', async () => {
      const tasks = new Task({ task: 'The Chronicles of Narnia' });
      const task = await tasks.save();
      const res = await requester
        .put(`/task/${task.id}`)
        .send({ task: '' });
      expect(res).to.have.status(400);
    });
  });

  describe('/DELETE/:ID task', () => {
    it('it should DELETE a task given the id', async () => {
      const tasks = new Task({ task: 'The Chronicles of Narnia' });
      const task = await tasks.save();
      const res = await requester
        .delete(`/task/${task.id}`);
      expect(res).to.have.status(200);
    });

    it('it should DELETE a task given the id', async () => {
      const res = await requester
        .delete('/task/5e285858a83f5218cc4e59db');
      expect(res).to.have.status(404);
    });
  });
});
